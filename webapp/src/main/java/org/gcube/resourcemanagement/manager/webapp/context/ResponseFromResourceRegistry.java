package org.gcube.resourcemanagement.manager.webapp.context;

import java.net.URI;
import java.util.Optional;

import org.gcube.common.gxrest.response.outbound.ErrorCode;


/**
 * The response to a {@link RequestToResourceRegistry}.
 * 
 * @author Manuele Simi (ISTI-CNR)
 *
 */
public class ResponseFromResourceRegistry {
	
	private boolean success = false;
	/**
	 * The message associated to the response
	 */
	private String message;
	
	private Exception exception;
	
	private ErrorCode code;

	/**
	 * The URI of the new context resource
	 */
	private URI location;

	protected ResponseFromResourceRegistry() {}
	
	public boolean wasSuccessful() {return success;}
	
	/**
	 * Gets the message associated to the response
	 * @return the message
	 */
	public Optional<String> getMessage() {
		return Optional.ofNullable(message);
	}

	/**
	 * Gets the location of the new resource.
	 * @return the location
	 */
	public URI getLocation() {
		return location;
	}
	/**
	 * Sets the message for the response.
	 * @param message the message 
	 */
	public ResponseFromResourceRegistry setMessage(String message) {
		this.message = message;
		return this;
	}
	
	/**
	 * The {@link Exception} returned by the RR.
	 * @return the exception
	 */
	public Optional<Exception> getException() {
		return Optional.ofNullable(success? null: this.exception);
	}

	/**
	 * Creates a response from an exception.
	 * @param e
	 * @return the response
	 */
	public static ResponseFromResourceRegistry fromException(Exception e) {
		ResponseFromResourceRegistry response = new ResponseFromResourceRegistry();
		response.success = false;
		response.exception = e;
		return response;
	}
	
	/**
	 * The {@link ErrorCode} occurred when preparing the response.
	 * @return the error code or null
	 */
	public Optional<ErrorCode> getErrorCode() {
		return Optional.ofNullable(success? null: this.code);
	}
	
	/**
	 * Creates a response from an error code.
	 * @param e
	 * @return the response
	 */
	public static ResponseFromResourceRegistry fromErrorCode(ErrorCode e) {
		ResponseFromResourceRegistry response = new ResponseFromResourceRegistry();
		response.success = false;
		response.code = e;
		return response;
	}
	
	/**
	 * Creates a failure response
	 * @return the response
	 */
	public static ResponseFromResourceRegistry newFailureResponse(String reason) {
		ResponseFromResourceRegistry response = new ResponseFromResourceRegistry();
		response.success = false;
		response.message = reason;
		return response;
	}

	/**
	 * Creates a success response with an associated message.
	 * @param message the message associated to the response
	 * @param context_id the uri of the new context
	 * @return the response
	 */
	public static ResponseFromResourceRegistry newSuccessResponseWithMessage(String message, URI context_id) {
		ResponseFromResourceRegistry response = newSuccessResponse(context_id);
		response.message = message;
		return response;
	}

	/**
	 * Creates a success response
	 * @param context_id the uri of the new context
	 * @return the response
	 */
	public static ResponseFromResourceRegistry newSuccessResponse(URI context_id) {
		ResponseFromResourceRegistry response = new ResponseFromResourceRegistry();
		response.success = true;
		response.location = context_id;
		return response;
	}

}
