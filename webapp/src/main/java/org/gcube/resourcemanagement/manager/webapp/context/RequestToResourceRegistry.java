package org.gcube.resourcemanagement.manager.webapp.context;

import java.util.Objects;

import org.gcube.common.gxrest.response.outbound.LocalCodeException;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClient;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClientFactory;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClientImpl;
import org.gcube.informationsystem.resourceregistry.context.ResourceRegistryContextClient;
import org.gcube.informationsystem.resourceregistry.context.ResourceRegistryContextClientFactory;
import org.gcube.informationsystem.resourceregistry.context.ResourceRegistryContextClientImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base request to an operation exposed by the Resource Registry.
 * 
 * @author Manuele Simi (ISTI-CNR)
 *
 */
public abstract class RequestToResourceRegistry {

	protected ResourceRegistryContextClient resourceRegistryContextClient;
	private ResourceRegistryClientImpl resourceResourceRegistryClient;
	protected static Logger logger = LoggerFactory.getLogger(RequestToResourceRegistry.class);

	/**
	 * Forces to use a RR instance at the given url.
	 * 
	 * @param forceURL
	 */
	public RequestToResourceRegistry forceURL(String forceURL) {
		this.resourceRegistryContextClient = new ResourceRegistryContextClientImpl(forceURL);
		this.resourceResourceRegistryClient = new ResourceRegistryClientImpl(forceURL);
		return this;
	}

	/**
	 * Validates the request.
	 * 
	 * @throws LocalCodeException 
	 */
	abstract public void validate() throws LocalCodeException;

	public ResponseFromResourceRegistry submit() {
		if (Objects.isNull(this.resourceRegistryContextClient))
			this.resourceRegistryContextClient = ResourceRegistryContextClientFactory.create();
		try {
			this.validate();
		} catch (LocalCodeException e) {
			ResponseFromResourceRegistry response = ResponseFromResourceRegistry.
					fromErrorCode(e);
			return response;
		}
		return this.validateResponse(this.send());
	}

	/**
	 * Gives a chance to sub-classes to validate the response.
	 * 
	 * @param response
	 * @return
	 */
	protected ResponseFromResourceRegistry validateResponse(ResponseFromResourceRegistry response) {
		// by default, returns the response as it is
		return response;
	}
	
	/**
	 * @return the context client
	 */
	ResourceRegistryContextClient getContextClient() {
		if (Objects.nonNull(this.resourceRegistryContextClient)) {
			return this.resourceRegistryContextClient;
		} else {
			return ResourceRegistryContextClientFactory.create();
		}
	}
	
	/**
	 * @return the registry client
	 */
	ResourceRegistryClient getRegistryClient() {
		if (Objects.nonNull(this.resourceResourceRegistryClient)) {
			return this.resourceResourceRegistryClient;
		} else {
			return ResourceRegistryClientFactory.create();
		}
	}

	/**
	 * Submits the request to the RR.
	 * 
	 * @return the response from the service.
	 */
	abstract protected ResponseFromResourceRegistry send();

}
