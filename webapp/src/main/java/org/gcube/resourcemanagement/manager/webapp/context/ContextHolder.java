package org.gcube.resourcemanagement.manager.webapp.context;


import org.gcube.informationsystem.model.reference.entities.Context;
import org.gcube.informationsystem.utils.ISMapper;


/**
 * Hold a context before sending it to the Resource Registry.
 * 
 * @author Manuele Simi (ISTI-CNR)
 *
 */
public class ContextHolder {

	private final Context inputContext;
	
	/**
	 * Creates a new holder for the serialized context.
	 *
	 * @param serialization a serialization in the JSON format
	 */
	public ContextHolder(String serialization) {
		try {
			this.inputContext = ISMapper.unmarshal(Context.class, serialization);
		} catch (Exception e) {
			throw new IllegalArgumentException("The context is syntactically not valid");
		}
	}

	protected Context getContext() {
		return inputContext;
	}

}
