/**
 * 
 * Logic to manage contexts within the webapp.
 * 
 * @author Manuele Simi (ISTI-CNR)
 *
 */
package org.gcube.resourcemanagement.manager.webapp.context;