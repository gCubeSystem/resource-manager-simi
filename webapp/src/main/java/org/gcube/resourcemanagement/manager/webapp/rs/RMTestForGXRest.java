package org.gcube.resourcemanagement.manager.webapp.rs;

import static org.gcube.resourcemanagement.manager.io.rs.RMContextsAccess.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.gcube.common.authorization.library.provider.CalledMethodProvider;
import org.gcube.common.gxrest.response.outbound.GXOutboundErrorResponse;
import org.gcube.common.gxrest.response.outbound.GXOutboundSuccessResponse;
import org.gcube.informationsystem.model.impl.entities.ContextImpl;
import org.gcube.informationsystem.model.reference.entities.Context;
import org.gcube.informationsystem.utils.ISMapper;
import org.gcube.resourcemanagement.manager.io.rs.RMContextDoesNotExistException;
import org.gcube.resourcemanagement.manager.webapp.ResourceInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * Tests for gxrest
 * 
 * @author Manuele Simi (ISTI CNR)
 *
 */
@Path(GXREST_ROOT)
public class RMTestForGXRest {

	private static Logger logger = LoggerFactory.getLogger(RMTestForGXRest.class);

	/** e.g. DELETE
	 * /resource-manager/gxrest/UUID?rrURL=http://registry:port//resource-
	 * registry
	 */
	@DELETE
	@Path("{" + CONTEXT_UUID_PARAM + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public Response delete(@PathParam(CONTEXT_UUID_PARAM) String uuid) {
		CalledMethodProvider.instance.set(String.format("DELETE /%s/%s", APPLICATION_PATH, GXREST_ROOT));
		methodOne();
		return null;
	 }
	
	/*
	 * e.g. POST
	 * /resource-manager/context?rrURL=http://registry:port//resource-registry
	 */
	@POST
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public Response create(@QueryParam(FORCE_RRURL_PARAM) String rrURL, String context) throws URISyntaxException {
		CalledMethodProvider.instance.set(String.format("POST /%s/%s", APPLICATION_PATH, GXREST_ROOT));
		logger.info("Requested to create resource with context {}", context);
		//logger.info("Force URL: " + rrURL);
		Context newContext = null;
		try {
			newContext = ISMapper.unmarshal(Context.class, context);
		} catch (JsonParseException e) {
			e.printStackTrace();
			GXOutboundErrorResponse.throwExceptionWithTrace(e, 5);
		} catch (JsonMappingException e) {
			GXOutboundErrorResponse.throwExceptionWithTrace(e, 5);
		} catch (IOException e) {
			GXOutboundErrorResponse.throwExceptionWithTrace(e, 5);
		}
		return GXOutboundSuccessResponse.newCREATEResponse(new URI(newContext.getHeader().getUUID().toString())).withContent("Context successfully created.")
				.ofType(MediaType.TEXT_PLAIN).build();
	}/*
	 
	/*
	 * e.g. PUT
	 * /resource-manager/context?rrURL=http://registry:port//resource-registry
	 */
	@PUT
	@Path("{" + CONTEXT_UUID_PARAM + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public Response update(@PathParam(CONTEXT_UUID_PARAM) String id, @QueryParam(FORCE_RRURL_PARAM) String rrURL) throws URISyntaxException, JsonProcessingException {
		CalledMethodProvider.instance.set(String.format("GET /%s/%s", APPLICATION_PATH, GXREST_ROOT));
		logger.info("Requested to update resource with id {}", id);
		logger.info("Force URL: " + rrURL);
		Context newContext = new ContextImpl("gxTest");
		newContext.getHeader().setUUID(UUID.fromString("6f86dc81-2f59-486b-8aa9-3ab5486313c4"));
		return GXOutboundSuccessResponse.newCREATEResponse(new URI(newContext.getHeader().getUUID().toString())).withContent(ISMapper.marshal(newContext))
				.ofType(MediaType.APPLICATION_JSON).build();
	}
	 
	
	private void methodOne() { 
		methodTwo();
	}
	
	private void methodTwo() {
		
		//something fails here
		logger.error("method 2 failed");
		GXOutboundErrorResponse.throwExceptionWithTrace(new RMContextDoesNotExistException("Error in methodTwo"),3,Response.Status.BAD_REQUEST);
	}
	
	private void methodThree() {
		
		//something fails here
		logger.error("method 3 failed");

		GXOutboundErrorResponse.throwException(new RMContextDoesNotExistException("Error in methodThree"));
	}
}
