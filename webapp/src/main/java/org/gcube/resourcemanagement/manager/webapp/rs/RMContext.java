package org.gcube.resourcemanagement.manager.webapp.rs;

import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.gcube.common.authorization.library.provider.CalledMethodProvider;
import org.gcube.common.gxrest.response.outbound.GXOutboundErrorResponse;
import org.gcube.common.gxrest.response.outbound.GXOutboundSuccessResponse;

import static org.gcube.resourcemanagement.manager.io.rs.RMContextsAccess.*;

import org.gcube.informationsystem.model.reference.entities.Context;
import org.gcube.resourcemanagement.manager.webapp.ResourceInitializer;
import org.gcube.resourcemanagement.manager.webapp.context.CreateRequest;
import org.gcube.resourcemanagement.manager.webapp.context.DeleteRequest;
import org.gcube.resourcemanagement.manager.webapp.context.ResponseFromResourceRegistry;
import org.gcube.resourcemanagement.manager.webapp.context.ContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.UUID;


/**
 * Resource methods for {@link org.gcube.informationsystem.model.reference.entities.Context}.
 * 
 * @author Manuele Simi (ISTI-CNR)
 *
 */
@Path(CONTEXT_ROOT)
public class RMContext {

	private static Logger logger = LoggerFactory.getLogger(RMContext.class);

	/*
	 * e.g. POST
	 * /resource-manager/context?rrURL=http://registry:port//resource-registry
	 */
	@POST
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public Response create(String json, @QueryParam(FORCE_RRURL_PARAM) String rrURL) {
		CalledMethodProvider.instance.set(String.format("POST /%s/%s", APPLICATION_PATH, CONTEXT_ROOT));
		logger.info("Requested to create context {} with json {}", Context.NAME, json);
		logger.info("Force URL: " + rrURL);
		ContextHolder holder = new ContextHolder(json); 
		ResponseFromResourceRegistry returned = CreateRequest.fromHolder(holder).forceURL(rrURL).submit();
		if (!returned.wasSuccessful()) {
			if (returned.getException().isPresent())
				GXOutboundErrorResponse.throwException(returned.getException().get());
			else if (returned.getErrorCode().isPresent())
				GXOutboundErrorResponse.throwErrorCode(returned.getErrorCode().get());
			else 
				GXOutboundErrorResponse.throwException(new Exception("Failed to create the context."));
		}
		return GXOutboundSuccessResponse.newCREATEResponse(returned.getLocation()).withContent("Context successfully created.")
				.ofType(MediaType.TEXT_PLAIN).build();
	}

	/*
	 * e.g. DELETE
	 * /resource-manager/context/UUID?rrURL=http://registry:port//resource-
	 * registry
	 */
	@DELETE
	@Path("{" + CONTEXT_UUID_PARAM + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public Response delete(@PathParam(CONTEXT_UUID_PARAM) String uuid, @QueryParam(FORCE_RRURL_PARAM) String rrURL) {
		CalledMethodProvider.instance.set(String.format("DELETE /%s/%s/ID", APPLICATION_PATH, CONTEXT_ROOT));
		logger.info("Requested to delete context with id {}", uuid);
		logger.info("Force URL: " + rrURL);
		ResponseFromResourceRegistry returned = DeleteRequest.fromUUID(UUID.fromString(uuid)).forceURL(rrURL).submit();
		if (!returned.wasSuccessful()) {
			if (returned.getException().isPresent())
				GXOutboundErrorResponse.throwException(returned.getException().get());
			else if (returned.getErrorCode().isPresent())
				GXOutboundErrorResponse.throwErrorCode(returned.getErrorCode().get());
			else
				GXOutboundErrorResponse.throwException(new Exception("Failed to delete the context."));
		}
		return GXOutboundSuccessResponse.newOKResponse().withContent("Context successfully deleted.")
				.ofType(MediaType.TEXT_PLAIN).build();
	}
}
