package org.gcube.resourcemanagement.manager.webapp;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.MediaType;

import org.gcube.resourcemanagement.manager.io.rs.RMContextsAccess;
import org.gcube.resourcemanagement.manager.webapp.rs.RMContext;
import org.gcube.resourcemanagement.manager.webapp.rs.RMTestForGXRest;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * @author Manuele Simi (ISTI - CNR)
 */
@ApplicationPath(RMContextsAccess.APPLICATION_PATH)
public class ResourceInitializer extends ResourceConfig  {

	public static final String APPLICATION_JSON_CHARSET_UTF_8 = MediaType.APPLICATION_JSON + ";charset=UTF-8";
	
	public ResourceInitializer(){
		//where jersey will find the operation's handler
		packages(RMContext.class.getPackage().toString(),RMTestForGXRest.class.getPackage().toString());
	}

}