package org.gcube.resourcemanagement.manager.webapp.context;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

import org.gcube.common.gxrest.response.outbound.LocalCodeException;
import org.gcube.informationsystem.model.reference.entities.Context;
import org.gcube.informationsystem.model.reference.relations.IsParentOf;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.context.ContextAlreadyPresentException;
import org.gcube.resourcemanagement.manager.io.rs.RMCreateContextCode;

/**
 * Create request for a new context.
 * 
 * @author Manuele Simi (ISTI-CNR)
 *
 */
public final class CreateRequest extends RequestToResourceRegistry {

	private Context context;

	private CreateRequest(Context context) {
		this.context = context;
	}

	public static CreateRequest fromHolder(ContextHolder holder) {
		Objects.requireNonNull(holder);
		return new CreateRequest(holder.getContext());
	}

	/* (non-Javadoc)
	 * @see org.gcube.resourcemanagement.manager.webapp.context.RequestToResourceRegistry#validate()
	 */
	@Override
	public void validate() throws LocalCodeException {
		if (Objects.nonNull(this.context.getParent())) {
			try {
				IsParentOf<Context, Context> relationship = this.context.getParent();
				logger.info("Fetching parent with UUID: " + relationship.getSource().getHeader().getUUID().toString());
				Context parent = new Queries(this.getContextClient(), this.getRegistryClient()).fetchContext(relationship.getSource().getHeader().getUUID());
				if (Objects.isNull(parent)) {
					throw new LocalCodeException(RMCreateContextCode.CONTEXT_PARENT_DOES_NOT_EXIST);
				}
			} catch (Exception e) {
				throw new LocalCodeException(RMCreateContextCode.CONTEXT_PARENT_DOES_NOT_EXIST);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.gcube.resourcemanagement.manager.webapp.context.RequestToResourceRegistry#send()
	 */
	@Override
	protected ResponseFromResourceRegistry send() {
		try {
			Context created = this.getContextClient().create(context);
			if (Objects.nonNull(created)) {
				return ResponseFromResourceRegistry.newSuccessResponseWithMessage("Context successfully created.", new URI(created.getHeader().getUUID().toString()));
			} else {
				return ResponseFromResourceRegistry.newFailureResponse("Invalid response from the RR (null?)");
			}
		} catch (ContextAlreadyPresentException cape) {
			return ResponseFromResourceRegistry.fromErrorCode(RMCreateContextCode.CONTEXT_ALREADY_EXISTS);
		} catch (ResourceRegistryException e) {
			return ResponseFromResourceRegistry.fromErrorCode(RMCreateContextCode.GENERIC_ERROR_FROM_RR);
		} catch (URISyntaxException e) {
			return ResponseFromResourceRegistry.fromErrorCode(RMCreateContextCode.GENERIC_ERROR_FROM_RR);
		}
	}

}
