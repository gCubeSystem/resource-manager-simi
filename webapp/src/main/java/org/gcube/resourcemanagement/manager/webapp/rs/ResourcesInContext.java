package org.gcube.resourcemanagement.manager.webapp.rs;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.gcube.common.gxrest.response.outbound.GXOutboundSuccessResponse;

import static org.gcube.resourcemanagement.manager.io.rs.RMContextsAccess.CONTEXT_UUID_PARAM;
import static org.gcube.resourcemanagement.manager.io.rs.ResourcesInContextAccess.*;


/**
 * Methods for {@link org.gcube.informationsystem.model.entity.Resource} operations.
 * 
 * @author Manuele Simi (ISTI-CNR)
 *
 */
@Path(ROOT)
public class ResourcesInContext {


	@PUT
	@Path("{" + CONTEXT_UUID_PARAM + "}/{"+ RESOURCE_UUID_PARAM +"}")
	public Response addToContext(@PathParam(CONTEXT_UUID_PARAM) String contextUUID, 
							 @PathParam(CONTEXT_UUID_PARAM) String resourceUUID) {
		
		return GXOutboundSuccessResponse.newCREATEResponse(null).build();
	}
}
