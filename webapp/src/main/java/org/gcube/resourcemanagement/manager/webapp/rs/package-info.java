/**
 * Signatures of the REST methods for the Resource Manager webapp.
 * 
 * @author Manuele Simi (ISTI - CNR)
 *
 */
package org.gcube.resourcemanagement.manager.webapp.rs;