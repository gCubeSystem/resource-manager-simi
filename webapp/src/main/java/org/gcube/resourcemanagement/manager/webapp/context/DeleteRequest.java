package org.gcube.resourcemanagement.manager.webapp.context;

import java.util.Objects;
import java.util.UUID;

import org.gcube.common.gxrest.response.outbound.LocalCodeException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.resourcemanagement.manager.io.rs.RMDeleteContextCode;

/**
 * Delete request for a context.
 * 
 * @author Manuele Simi (ISTI CNR)
 *
 */
public final class DeleteRequest extends RequestToResourceRegistry {

	final UUID context;
	
	private DeleteRequest(UUID context) {
		this.context = context;
	}

	public static DeleteRequest fromUUID(UUID context) {
		Objects.requireNonNull(context);
		return new DeleteRequest(context);
	}
	/* (non-Javadoc)
	 * @see org.gcube.resourcemanagement.manager.webapp.context.RequestToResourceRegistry#validate()
	 */
	@Override
	public void validate() throws LocalCodeException {
		Queries queries = new Queries(this.getContextClient(), this.getRegistryClient());
		if (!queries.contextExists(context)) {
			throw new LocalCodeException(RMDeleteContextCode.CONTEXT_DOES_NOT_EXIST);
		}
		if (!queries.isContextEmpty(context)) {
			throw new LocalCodeException(RMDeleteContextCode.CONTEXT_IS_NOT_EMPTY);
		}
	}

	/* (non-Javadoc)
	 * @see org.gcube.resourcemanagement.manager.webapp.context.RequestToResourceRegistry#send()
	 */
	@Override
	protected ResponseFromResourceRegistry send() {
		try {
			boolean deleted = this.getContextClient().delete(context);
			if (!deleted) {
				return ResponseFromResourceRegistry.fromErrorCode(RMDeleteContextCode.GENERIC_ERROR_FROM_RR);
			}
		} catch (ResourceRegistryException e) {
			return ResponseFromResourceRegistry.fromErrorCode(RMDeleteContextCode.GENERIC_ERROR_FROM_RR);
		}

		return ResponseFromResourceRegistry.newSuccessResponseWithMessage("Context successfully deleted", null);
	}

}
