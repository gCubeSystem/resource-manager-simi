package org.gcube.resourcemanagement.manager.webapp.context;

import java.util.Objects;
import java.util.UUID;

import org.gcube.informationsystem.model.reference.entities.Context;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.context.ContextNotFoundException;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClient;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClientFactory;
import org.gcube.informationsystem.resourceregistry.context.ResourceRegistryContextClient;
import org.gcube.informationsystem.resourceregistry.context.ResourceRegistryContextClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Queries submitted within the context resource.
 * 
 * @author Manuele Simi (ISTI CNR)
 *
 */
final class Queries {
	
	private final ResourceRegistryContextClient cclient;
	private final ResourceRegistryClient rclient;

	protected static Logger logger = LoggerFactory.getLogger(Queries.class);

	protected Queries() {
		cclient = ResourceRegistryContextClientFactory.create();
		rclient = ResourceRegistryClientFactory.create();
	}
	
	/**
	 * Makes the queries with the given client.
	 * 
	 * @param cclient
	 */
	protected Queries(ResourceRegistryContextClient cclient, ResourceRegistryClient rclient) {
		this.cclient = cclient;
		this.rclient =rclient;
	}
	
	/**
	 *  Returns {@code true} if the provided context exists
	 *  otherwise returns {@code false}.
	 *  
	 * @param context
	 * @return {@code true} if the provided context exists
	 * otherwise {@code false}
	 * */
	protected boolean contextExists(UUID context) {
		try {
			Context results = this.cclient.read(context);
			return Objects.nonNull(results);
		} catch (ContextNotFoundException e) {
			logger.warn("Context not found: " + e.getMessage());
			return false;
		} catch (Exception e) {
			logger.warn("Failed to query the Resource Registry: " + e.getMessage());
			return false;
		}
	}
	
	/**
	 * Fetches the resources context.
	 * 
	 * @param context the resource context to fetch
	 * @return
	 */
	protected Context fetchContext(UUID context) {
		try {
			return this.cclient.read(context);
		} catch (ResourceRegistryException e) {
			logger.warn("Failed to query the Resource Registry: " + e.getMessage());
			return null;
		}
	}


	/**
	 * Detects if the context has no resources.
	 * 
	 * @param context the context to check 
	 * @return true if the content is empty.
	 */
	protected boolean isContextEmpty(UUID context) {
		try {
			return this.rclient.getInstances(Resource.class, true).size() == 0;
		} catch (ResourceRegistryException e) {
			logger.warn("Failed to query the Resource Registry: " + e.getMessage());
			return false;
		}
	}

}
