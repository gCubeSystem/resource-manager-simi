package org.gcube.resourcemanagement.manager.webapp.context;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.gcube.common.authorization.client.Constants;
import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.gxrest.response.inbound.GXInboundResponse;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resourcemanagement.manager.io.rs.RMContextsAccess;
import org.gcube.resourcemanagement.manager.webapp.rs.RMContext;
import org.gcube.resourcemanagement.manager.webapp.rs.RMTestForGXRest;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test cases for the Context methods.
 * 
 * @author Manuele Simi (ISTI-CNR)
 */
@RunWith(BlockJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GXRestTest extends JerseyTest {

	private final static String context1 = "ContextA";

	private final static String context2 = "ContextB";

	private final static String context3 = "ContextC";

	public static final UUID context1UUID = UUID.fromString("5f86dc81-2f59-486b-8aa9-3ab5486313c4");

	public static final UUID context2UUID = UUID.fromString("6f86dc81-2f59-486b-8aa9-3ab5486313c4");

	public static final UUID context3UUID = UUID.fromString("7f86dc81-2f59-486b-8aa9-3ab5486313c4");

	private final static String RR = "http://manuele-registry.dev.d4science.org/resource-registry";

	public static String DEFAULT_TEST_SCOPE = "";

	private static boolean skipTest = false;

	private static final Logger logger = LoggerFactory.getLogger(GXRestTest.class);

	static {
		Properties properties = new Properties();
		try (InputStream input = GXRestTest.class.getClassLoader().getResourceAsStream("token.properties")) {
			// load the properties file
			properties.load(input);
		} catch (IOException e) {
			skipTest = true;
			// throw new RuntimeException(e);
		}

		DEFAULT_TEST_SCOPE = properties.getProperty("DEFAULT_SCOPE_TOKEN");
		if (DEFAULT_TEST_SCOPE.isEmpty())
			skipTest = true;
	}

	@BeforeClass
	public static void beforeClass() throws Exception {
		setContext(DEFAULT_TEST_SCOPE);
	}

	public static void setContext(String token) throws Exception {
		if (DEFAULT_TEST_SCOPE.isEmpty()) {
			skipTest = true;
			return;
		}
		SecurityTokenProvider.instance.set(token);
		ScopeProvider.instance.set(getCurrentScope(token));
	}

	public static String getCurrentScope(String token) throws Exception {
		AuthorizationEntry authorizationEntry = Constants.authorizationService().get(token);
		String context = authorizationEntry.getContext();
		return context;
	}

	@AfterClass
	public static void afterClass() {
		SecurityTokenProvider.instance.reset();
		ScopeProvider.instance.reset();
	}

	@Override
	protected Application configure() {
		return new ResourceConfig(RMTestForGXRest.class);
	}

		/**
	 * Test method for
	 * {@link RMContext#delete(String, String)}
	 * 
	 * @throws Exception
	 */
	@Test
	public void step1_ExceptionWithStacktrace() throws Exception {
		if (skipTest)
			return;

		Response delete = target(RMContextsAccess.GXREST_ROOT).path(context2UUID.toString()).request().delete();
		assertNotNull(delete);
		GXInboundResponse response = new GXInboundResponse(delete);
		if (response.hasOKCode()) {

		} else {
			assertEquals("Unexpected returned code. Reason: " + response.getHTTPCode(),
					Status.UNAUTHORIZED.getStatusCode(), delete.getStatus());
			if (response.hasException()) {
				try {
					response.getException().printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
					throw e;
				}
			}
		}
	}

}
