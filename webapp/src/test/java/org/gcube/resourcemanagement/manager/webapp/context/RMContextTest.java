package org.gcube.resourcemanagement.manager.webapp.context;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.gcube.common.authorization.client.Constants;
import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.gxrest.response.inbound.GXInboundResponse;
import org.gcube.common.gxrest.response.outbound.CodeFinder;
import org.gcube.common.gxrest.response.outbound.ErrorCode;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.informationsystem.model.impl.entities.ContextImpl;
import org.gcube.informationsystem.model.reference.entities.Context;
import org.gcube.informationsystem.resourceregistry.context.ResourceRegistryContextClient;
import org.gcube.informationsystem.resourceregistry.context.ResourceRegistryContextClientImpl;
import org.gcube.informationsystem.utils.ISMapper;
import org.gcube.resourcemanagement.manager.io.rs.RMContextsAccess;
import org.gcube.resourcemanagement.manager.io.rs.RMCreateContextCode;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * Test cases for the Context methods.
 * 
 * @author Manuele Simi (ISTI-CNR)
 */
@RunWith(BlockJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RMContextTest extends JerseyTest {

	private final static String context1 = "ContextLLLLL";

	private final static String context2 = "ContextB";

	private final static String context3 = "ContextC";

	public static final UUID context1UUID = UUID.fromString("b252e6ce-8b9a-4142-9bca-2ada9171034b");

	public static final UUID context2UUID = UUID.fromString("6f86dc81-2f59-486b-8aa9-3ab5486313c4");

	public static final UUID context3UUID = UUID.fromString("7f86dc81-2f59-486b-8aa9-3ab5486313c4");

	/**
	 * Resource Registry to contact
	 */
	private static String RR = "";

	public static String DEFAULT_TEST_SCOPE = "";

	private static boolean skipTest = false;

	private static final Logger logger = LoggerFactory.getLogger(RMContextTest.class);

	static {
		Properties properties = new Properties();
		try (InputStream input = RMContextTest.class.getClassLoader().getResourceAsStream("token.properties")) {
			// load the properties file
			properties.load(input);
		} catch (IOException e) {
			skipTest = true;
			// throw new RuntimeException(e);
		}

		DEFAULT_TEST_SCOPE = properties.getProperty("DEFAULT_SCOPE_TOKEN");
		if (DEFAULT_TEST_SCOPE.isEmpty())
			skipTest = true;

		RR = properties.getProperty("RR");
		if (RR.isEmpty())
			skipTest = true;
	}

	@BeforeClass
	public static void beforeClass() throws Exception {
		setContext(DEFAULT_TEST_SCOPE);
	}

	static void setContext(String token) throws Exception {
		if (DEFAULT_TEST_SCOPE.isEmpty()) {
			skipTest = true;
			return;
		}
		SecurityTokenProvider.instance.set(token);
		ScopeProvider.instance.set(getCurrentScope(token));
	}

	static String getCurrentScope(String token) throws Exception {
		AuthorizationEntry authorizationEntry = Constants.authorizationService().get(token);
		String context = authorizationEntry.getContext();
		return context;
	}

	@AfterClass
	public static void afterClass() {
		SecurityTokenProvider.instance.reset();
		ScopeProvider.instance.reset();
	}

	@Override
	protected Application configure() {
		return new ResourceConfig(RMContextsAccess.class);
	}

	/**
	 * Test method for marshal/unmarshal contexts.
	 */
	@Test
	public void step0_Context() {
		Context newContext = new ContextImpl(context1);
		newContext.getHeader().setUUID(context1UUID);
		try {
			Context uContext = ISMapper.unmarshal(Context.class, ISMapper.marshal(newContext));
			assertNotNull(uContext);
			assertEquals("Invalid context name", context1, uContext.getName());
			assertEquals("Invalid context UUID", context1UUID, uContext.getHeader().getUUID());
		} catch (IOException e) {
			e.printStackTrace();
			assertFalse("Failed to unmarshal the context.", false);
		}

	}

	/**
	 * Test method for
	 * {@link org.gcube.resourcemanagement.manager.webapp.rs.RMContext#create(String, String)}.
	 * 
	 * @throws Exception
	 */
	@Test
	public void step1_Create() throws Exception {
		if (skipTest)
			return;
		Context newContext = new ContextImpl(context1, context1UUID);
		try {
			System.out.print(ISMapper.marshal(newContext));
			Response create = target("context").queryParam(RMContextsAccess.FORCE_RRURL_PARAM, RR).request()
					.post(Entity.entity(ISMapper.marshal(newContext), MediaType.APPLICATION_JSON + ";charset=UTF-8"));
			assertNotNull(create);
			GXInboundResponse response = new GXInboundResponse(create);

			logger.info("RM returned status: " + create.getStatusInfo());
			if (response.hasCREATEDCode()) {
				logger.info("RM says: " + response.getMessage());
			} else {
				assertEquals("Unexpected returned code. Reason: " + create.getStatusInfo().getReasonPhrase(),
						Status.NOT_ACCEPTABLE.getStatusCode(), response.getHTTPCode());
				if (response.hasException()) {
					try {
						throw response.getException();
					} catch (Exception e) {
						e.printStackTrace();
						throw e;
					}
				} else if (response.hasErrorCode()) {
					ErrorCode code = response.getErrorCode();
					RMCreateContextCode realCode = org.gcube.common.gxrest.response.outbound.CodeFinder.findAndConvert(code, RMCreateContextCode.values());
					assertEquals(RMCreateContextCode.CONTEXT_ALREADY_EXISTS, realCode);
				} else {
					assertTrue("Invalid error response returned.", true);
				}
			}

		} catch (JsonProcessingException e) {
			assertFalse("Failed to marshal the context.", false);
		}
	}

	/**
	 * Test method for
	 * {@link org.gcube.resourcemanagement.manager.webapp.rs.RMContext#create(String, String)}
	 * when the parent does not exist.
	 */
	@Test
	public void step2_CreateWithInvalidParent() {
		if (skipTest)
			return;
		Context newContext = new ContextImpl(context2, context2UUID);
		newContext.setParent(new ContextImpl("DoNotExist"));
		try {
			Response create = target(RMContextsAccess.CONTEXT_ROOT).queryParam(RMContextsAccess.FORCE_RRURL_PARAM, RR)
					.request()
					.post(Entity.entity(ISMapper.marshal(newContext), MediaType.APPLICATION_JSON + ";charset=UTF-8"));
			GXInboundResponse response = new GXInboundResponse(create);

			assertTrue("Unexpected returned code. Reason: " + create.getStatusInfo().getReasonPhrase(),
					response.hasNOT_ACCEPTABLECode());
			if (response.hasErrorCode()) {
				RMCreateContextCode realCode = CodeFinder.findAndConvert(response.getErrorCode(),
						RMCreateContextCode.values());
				assertEquals(RMCreateContextCode.CONTEXT_PARENT_DOES_NOT_EXIST,realCode);
			}
		} catch (JsonProcessingException e) {
			assertFalse("Failed to marshal the context.", false);
		}
	}

	/**
	 * Test method for
	 * {@link org.gcube.resourcemanagement.manager.webapp.rs.RMContext#create(String, String)}.
	 * when the parent does not exist.
	 */
	@Test
	public void step3_CreateWithParent() {
		if (skipTest)
			return;
		ResourceRegistryContextClient client = new ResourceRegistryContextClientImpl(RR);
		Context newContext = new ContextImpl(context2, context2UUID);
		Queries queries = new Queries();
		newContext.setParent(context1UUID);
		try {
			Response create = target(RMContextsAccess.CONTEXT_ROOT).queryParam(RMContextsAccess.FORCE_RRURL_PARAM, RR)
					.request()
					.post(Entity.entity(ISMapper.marshal(newContext), MediaType.APPLICATION_JSON + ";charset=UTF-8"));
			assertNotNull(create);
			GXInboundResponse response = new GXInboundResponse(create);
			if (response.hasCREATEDCode()) {
				System.out.println("RM says: " + response.getMessage());
			} else {
				assertTrue("Unexpected returned code. Reason: " + response.getHTTPCode(),
						response.hasNOT_ACCEPTABLECode());
			}
		} catch (JsonProcessingException e) {
			assertFalse("Failed to marshal the context.", false);
		}
	}

	/**
	 * Test method for
	 * {@link org.gcube.resourcemanagement.manager.webapp.rs.RMContext#delete(String, String)}
	 * 
	 * @throws Exception
	 */
	@Test
	public void step4_Delete() throws Exception {
		if (skipTest)
			return;

		Response delete = target(RMContextsAccess.CONTEXT_ROOT).path(context2UUID.toString())
				.queryParam(RMContextsAccess.FORCE_RRURL_PARAM, RR).request().delete();
		assertNotNull(delete);
		GXInboundResponse response = new GXInboundResponse(delete);
		if (response.hasOKCode()) {

		} else {
			assertEquals("Unexpected returned code. Reason: " + delete.getStatusInfo().getReasonPhrase(),
					Status.NOT_ACCEPTABLE.getStatusCode(), delete.getStatus());
			if (response.hasException()) {
				try {
					throw response.getException();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw e;
				}
			}
		}
	}

	/**
	 * Test method for
	 * {@link org.gcube.resourcemanagement.manager.webapp.rs.RMContext#delete(String, String)}
	 */
	@Test
	public void step5_DeleteParent() {
		if (skipTest)
			return;
		try {
			Response delete = target(RMContextsAccess.CONTEXT_ROOT).path(context1UUID.toString())
					.queryParam(RMContextsAccess.FORCE_RRURL_PARAM, RR).request().delete();
			assertNotNull(delete);
			GXInboundResponse response = new GXInboundResponse(delete);
			int status = response.getHTTPCode();
			logger.info("Returnes status: " + status);
			if (!response.hasOKCode()) {
				assertTrue("Unexpected returned code. Reason: " + response.getHTTPCode(),
						response.hasNOT_ACCEPTABLECode());	
			}
			
		} catch (Exception e) {
			assertFalse("Failed to delete the context.", false);
		}
	}
}
