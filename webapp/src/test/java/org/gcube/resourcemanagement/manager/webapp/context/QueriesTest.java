package org.gcube.resourcemanagement.manager.webapp.context;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.gcube.common.authorization.client.Constants;
import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.informationsystem.model.reference.entities.Context;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClient;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClientImpl;
import org.gcube.informationsystem.resourceregistry.context.ResourceRegistryContextClient;
import org.gcube.informationsystem.resourceregistry.context.ResourceRegistryContextClientImpl;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.MethodSorters;

/**
 * Test cases for {@link Queries}
 *
 * @author Manuele Simi (ISTI CNR)
 */
@RunWith(BlockJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class QueriesTest {

    /**
     * Resource Registry to contact
     */
    private static String RR = "";

    public static String DEFAULT_TEST_SCOPE = "";

    public static final UUID context1UUID = RMContextTest.context1UUID;

    private static boolean skipTest = false;

    static {
        Properties properties = new Properties();
        try (InputStream input = RMContextTest.class.getClassLoader().getResourceAsStream("token.properties")) {
            // load the properties file
            properties.load(input);
        } catch (IOException e) {
            skipTest = true;
            //throw new RuntimeException(e);
        }

        DEFAULT_TEST_SCOPE = properties.getProperty("DEFAULT_SCOPE_TOKEN");
        if (DEFAULT_TEST_SCOPE.isEmpty())
            skipTest = true;
        RR = properties.getProperty("RR");
        if (RR.isEmpty())
            skipTest = true;
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        setContext(DEFAULT_TEST_SCOPE);
    }

    public static void setContext(String token) throws Exception {
        if (DEFAULT_TEST_SCOPE.isEmpty()) {
            skipTest = true;
            return;
        }
        SecurityTokenProvider.instance.set(token);
        ScopeProvider.instance.set(getCurrentScope(token));
    }

    public static String getCurrentScope(String token) throws Exception {
        AuthorizationEntry authorizationEntry = Constants.authorizationService().get(token);
        String context = authorizationEntry.getContext();
        return context;
    }

    @AfterClass
    public static void afterClass() {
        SecurityTokenProvider.instance.reset();
        ScopeProvider.instance.reset();
    }

    /**
     * Test method for {@link org.gcube.resourcemanagement.manager.webapp.context.Queries#contextExists(UUID)}.
     */
    @Test
    public void testContextExists() {
        if (skipTest)
            return;
        ResourceRegistryContextClient cclient = new ResourceRegistryContextClientImpl(RR);
        ResourceRegistryClient rclient = new ResourceRegistryClientImpl(RR);
        Queries queries = new Queries(cclient, rclient);
        assertTrue("Context does exist, but it should not", queries.contextExists(RMContextTest.context1UUID));
    }

    /**
     * Test method for {@link org.gcube.resourcemanagement.manager.webapp.context.Queries#fetchContext(UUID)} .
     */
    @Test
    public void testFetchContext() {
        if (skipTest)
            return;
        ResourceRegistryContextClient cclient = new ResourceRegistryContextClientImpl(RR);
        ResourceRegistryClient rclient = new ResourceRegistryClientImpl(RR);
        Queries queries = new Queries(cclient, rclient);
        Context context = queries.fetchContext(context1UUID);
        assertNotNull("Context does not exist.", context);
        assertEquals("Unexpected uuid for context", context1UUID, context.getHeader().getUUID());
    }

}
