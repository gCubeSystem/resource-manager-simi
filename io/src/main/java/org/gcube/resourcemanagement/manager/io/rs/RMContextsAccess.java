package org.gcube.resourcemanagement.manager.io.rs;

/**
 * REST paths and parameters for the RMContext resource exposed by the webapp.
 * 
 * @author Manuele Simi (ISTI - CNR)
 *
 */
public class RMContextsAccess {
	 
	public static final String APPLICATION_PATH = "/resource-manager";

	public static final String CONTEXT_ROOT = "context";
	
	public static final String CREATE_PATH_PART = "create";
	
	public static final String DELETE_PATH_PART = "delete";
	
	public static final String GXREST_ROOT = "gxrest";

	
	
	/* PARAMETERS FOR SOME REQUESTS*/
	
	/**
	 * Force the service to use the ResourceRegistry instance at the given URL.
	 */
	public static final String FORCE_RRURL_PARAM = "rrURL";
	
	/**
	 * The identifier of the context on which the method will operate.
	 */
	public static final String CONTEXT_UUID_PARAM = "ContextUUID";


	

}
