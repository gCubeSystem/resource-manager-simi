package org.gcube.resourcemanagement.manager.io.rs;

import org.gcube.common.gxrest.response.outbound.ErrorCode;
/**
 * Error codes returned by the delete method of the context resource.
 * 
 * @author Manuele Simi (ISTI CNR)
 *
 */
public enum RMDeleteContextCode implements ErrorCode {
	GENERIC_ERROR_FROM_RR(1, "The Resource Registry returned an error."),
	CONTEXT_DOES_NOT_EXIST(2, "The context does not exist."),
	CONTEXT_IS_NOT_EMPTY(3, "The context is not empty. It cannot be deleted. Use force parameter to delete.");

	private int id;
	private String msg;
		
	private RMDeleteContextCode(int id, String msg) {
		this.id = id;
		this.msg = msg;
	}
	/* (non-Javadoc)
	 * @see org.gcube.resourcemanagement.manager.io.codeexceptions.ErrorCode#getId()
	 */
	public int getId() {
		return this.id;
	}
	
	/* (non-Javadoc)
	 * @see org.gcube.resourcemanagement.manager.io.codeexceptions.ErrorCode#getMessage()
	 */
	public String getMessage() {
		return this.msg;
	}

}
