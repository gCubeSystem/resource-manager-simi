package org.gcube.resourcemanagement.manager.io.rs;

import org.gcube.common.gxrest.response.outbound.ErrorCode;

/**
 * Error codes returned by the create method of the context resource.
 * 
 * @author Manuele Simi (ISTI CNR)
 *
 */
public enum RMCreateContextCode implements ErrorCode {

	INVALID_METHOD_REQUEST(0, "The request is invalid."), 
	MISSING_PARAMETER(1,"Required query parameter is missing."), 
	MISSING_HEADER(2, "Required header is missing."), 
	CONTEXT_ALREADY_EXISTS(3, "Context already exists at the same level of the hierarchy."),
	CONTEXT_PARENT_DOES_NOT_EXIST(4, "Failed to validate the request. The request was not submitted to the Resource Registry."),
	INVALID_REQUEST_FOR_RR(5, "Failed to validate the request. The request was not submitted to the Resource Registry."),
	GENERIC_ERROR_FROM_RR(6, "The Resource Registry returned an error."),
	INVALID_RESPONSE_R_FROM_RR(7, "The Resource Registry returned an invalid response.");
	;

	private int id;
	private String msg;
		
	private RMCreateContextCode(int id, String msg) {
		this.id = id;
		this.msg = msg;
	}
	/* (non-Javadoc)
	 * @see org.gcube.resourcemanagement.manager.io.codeexceptions.ErrorCode#getId()
	 */
	public int getId() {
		return this.id;
	}
	/* (non-Javadoc)
	 * @see org.gcube.resourcemanagement.manager.io.codeexceptions.ErrorCode#getMessage()
	 */
	public String getMessage() {
		return this.msg;
	}
}
