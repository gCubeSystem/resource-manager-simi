/**
 * Extensions/abstraction over the gCube resource model.
 * 
 * @author Manuele Simi (ISTI - CNR)
 *
 */
package org.gcube.resourcemanagement.manager.io.extendedmodel;