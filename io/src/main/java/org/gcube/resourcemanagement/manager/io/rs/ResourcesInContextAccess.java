package org.gcube.resourcemanagement.manager.io.rs;

/**
 * Paths and parameters for the ResourcesInContext collection of resources.
 * 
 * @author Manuele Simi (ISTI - CNR)
 *
 */
public class ResourcesInContextAccess {
	public static final String ROOT = "resource";
	public static final String CONTEXT_UUID_PARAM = "ContextUUID";
	public static final String RESOURCE_UUID_PARAM = "ResourceUUID";


}
